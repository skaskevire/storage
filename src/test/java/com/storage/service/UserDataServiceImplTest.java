package com.storage.service;

import static org.assertj.core.util.Arrays.array;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.storage.controller.entity.UserDataDto;
import com.storage.csv.entity.UserDataLine;
import com.storage.csv.reader.MultipartFileReader;
import com.storage.csv.validator.FileValidator;
import com.storage.factory.UserDataDtoFactory;
import com.storage.factory.UserDataFactory;
import com.storage.persistence.entity.UserData;
import com.storage.persistence.repository.UserDataRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class UserDataServiceImplTest {

  @Mock
  private MultipartFileReader multipartFileReader;
  @Mock
  private UserDataDtoFactory userDataDtoFactory;
  @Mock
  private UserDataRepository userDataRepository;
  @Mock
  private FileValidator fileValidator;
  @Mock
  private UserDataFactory userDataFactory;
  @Mock
  private MultipartFile multipartFile;

  @InjectMocks
  private UserDataServiceImpl userDataService;

  @Test
  void shouldUploadFile() {
    List<UserDataLine> lines = new ArrayList<>();
    UserDataLine headerRow = new UserDataLine(array("headerRow"));
    UserDataLine dataRow = new UserDataLine(array("dataRow"));
    lines.add(headerRow);
    lines.add(dataRow);
    when(multipartFileReader.read(multipartFile)).thenReturn(lines);
    when(fileValidator.isLineValid(dataRow)).thenReturn(true);

    userDataService.upload(multipartFile);

    verify(multipartFileReader).read(multipartFile);
    verify(fileValidator).validateFileStructure(lines);
    verify(fileValidator).isLineValid(dataRow);
    verify(fileValidator, Mockito.times(0)).isLineValid(headerRow);
    verify(userDataFactory).create(dataRow);
    verify(userDataRepository).saveAll(any());
  }

  @Test
  void shouldGetUserData() {
    Long pk = 1L;
    UserData userData = new UserData();
    UserDataDto userDataDto = new UserDataDto();
    when(userDataRepository.findById(pk)).thenReturn(Optional.of(userData));
    when(userDataDtoFactory.create(userData)).thenReturn(userDataDto);

    UserDataDto actual = userDataService.get(pk);

    assertEquals(userDataDto, actual);
    verify(userDataRepository).findById(1L);
    verify(userDataDtoFactory).create(userData);
  }

  @Test
  void shouldDeleteUserData() {
    userDataService.delete(1L);

    verify(userDataRepository).deleteById(1L);
  }

}
