package com.storage.csv.validator;

import static org.assertj.core.util.Arrays.array;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import com.storage.csv.entity.UserDataLine;
import com.storage.exception.BusinessException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FileValidatorTest {

  private FileValidator fileValidator = new FileValidator("yyyy-MM-dd'T'HH:mm:ss");
  private static final UserDataLine HEADERS = new UserDataLine(
      array("PRIMARY_KEY", "NAME", "DESCRIPTION", "UPDATED_TIMESTAMP"));
  private static final UserDataLine LINE = new UserDataLine(
      array("1", "name", "description", "2001-01-01T00:00:00"));
  private static final UserDataLine EMPTY_LINE = new UserDataLine(array());
  private List<UserDataLine> lines;

  @BeforeEach
  public void init() {
    lines = new ArrayList<>();
    lines.add(HEADERS);
    lines.add(LINE);
    lines.add(EMPTY_LINE);
  }

  @Test
  void shouldAcceptProperHeaders() {
    fileValidator.validateFileStructure(lines);
  }

  @Test
  void shouldDenyEmptyHeadersString() {
    lines.set(0, EMPTY_LINE);
    try {
      fileValidator.validateFileStructure(lines);
      fail();
    } catch (BusinessException ex) {
      assertEquals(ex.getMessage(), "Headers are empty!");
    }
  }

  @Test
  void shouldDenyCorruptedHeadersString() {
    UserDataLine corruptedHeaders =
        new UserDataLine(array(
            "PRIMARY_KEY",
            "NAME",
            "DESCRIPTION"));
    lines.set(0, corruptedHeaders);
    try {
      fileValidator.validateFileStructure(lines);
      fail();
    } catch (BusinessException ex) {
      assertEquals(ex.getMessage(),
          "Specified headers are invalid, expected headers are: " + Arrays.toString(HEADERS.getRows()));
    }
  }

  @Test
  void shouldDenyWhenRowsAreEmpty() {
    lines.clear();
    try {
      fileValidator.validateFileStructure(lines);
      fail();
    } catch (BusinessException ex) {
      assertEquals(ex.getMessage(),
          "Provided file is empty!");
    }
  }

  @Test
  void shouldDenyWhenEndingLineNotEmpty() {
    lines.set(2, LINE);
    try {
      fileValidator.validateFileStructure(lines);
      fail();
    } catch (BusinessException ex) {
      assertEquals(ex.getMessage(),
          "Ending line is not empty!");
    }
  }

  @Test
  void shouldAcceptProperLine() {
    assertTrue(fileValidator.isLineValid(LINE));
  }

  @Test
  void shouldDenyEmptyLine() {
    Assertions.assertFalse(fileValidator.isLineValid(EMPTY_LINE));
  }

  @Test
  void shouldDenyLineWithWrongNumberOfColumns() {
    UserDataLine corruptedLine = new UserDataLine(array("1", "name", "description"));
    Assertions.assertFalse(fileValidator.isLineValid(corruptedLine));
  }

  @Test
  void shouldDenyLineWithEmptyPK() {
    UserDataLine corruptedLine = new UserDataLine(
        array("", "name", "description", "2001-01-01T00:00:00"));
    Assertions
        .assertFalse(fileValidator.isLineValid(corruptedLine));
  }

  @Test
  void shouldDenyLineWithNotNumericPK() {
    UserDataLine corruptedLine = new UserDataLine(
        array("pk", "name", "description", "2001-01-01T00:00:00"));
    Assertions
        .assertFalse(fileValidator.isLineValid(corruptedLine));
  }

  @Test
  void shouldDenyLineWithEmptyUpdatedTimestamp() {
    UserDataLine corruptedLine = new UserDataLine(array("1", "name", "description", ""));
    Assertions.assertFalse(fileValidator.isLineValid(corruptedLine));
  }

  @Test
  void shouldDenyLineWithDifferentUpdatedTimestampFormat() {
    UserDataLine corruptedLine = new UserDataLine(array("1", "name", "description", "2001-01-01"));
    Assertions.assertFalse(fileValidator.isLineValid(corruptedLine));
  }
}
