package com.storage.csv.reader;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.storage.csv.entity.UserDataLine;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class MultipartFileReaderTest {

  @Mock
  private MultipartFile multipartFile;
  @InjectMocks
  private MultipartFileReader userDataListReader;

  @Test
  void shouldReadRows() throws IOException {
    when(multipartFile.getInputStream()).thenReturn(openIS("testdata.csv"));
    String[] headers = {"PRIMARY_KEY", "NAME", "DESCRIPTION", "UPDATED_TIMESTAMP"};
    String[] line = {"1", "name", "description", "2001-01-01T00:00:00"};
    String[] emptyLine = {""};
    List<UserDataLine> lines = userDataListReader.read(multipartFile);

    assertEquals(3, lines.size());
    assertArrayEquals(headers, lines.get(0).getRows());
    assertArrayEquals(line, lines.get(1).getRows());
    assertArrayEquals(emptyLine, lines.get(2).getRows());
  }

  @Test
  void shouldReturnEmptyListWhenFileEmpty() throws IOException {
    when(multipartFile.getInputStream()).thenReturn(openIS("emptytestdata.csv"));

    List<UserDataLine> lines = userDataListReader.read(multipartFile);

    assertEquals(0, lines.size());
  }

  private InputStream openIS(String fileName) throws FileNotFoundException {
    ClassLoader classLoader = this.getClass().getClassLoader();
    File file = new File(classLoader.getResource(fileName).getFile());
    assertTrue(file.exists());

    return new FileInputStream(file);
  }
}
