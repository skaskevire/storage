package com.storage.controller;

import com.storage.service.UserDataService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/owner/data")
public class OwnerController {

  private UserDataService userDataService;

  public OwnerController(UserDataService userDataService) {
    this.userDataService = userDataService;
  }

  @DeleteMapping("/{primaryKey}")
  public void deleteByPrimaryKey(@RequestParam Long primaryKey) {
    userDataService.delete(primaryKey);
  }
}
