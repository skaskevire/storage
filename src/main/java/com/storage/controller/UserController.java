package com.storage.controller;

import com.storage.controller.entity.UserDataDto;
import com.storage.service.UserDataService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/user/data")
public class UserController {

  private UserDataService userDataService;

  public UserController(UserDataService userDataService) {
    this.userDataService = userDataService;
  }

  @PostMapping
  public void upload(@RequestParam("file") MultipartFile file) {
    userDataService.upload(file);
  }

  @GetMapping("/{primaryKey}")
  public ResponseEntity<UserDataDto> getByPrimaryKey(@RequestParam Long primaryKey) {
    return new ResponseEntity<>(userDataService.get(primaryKey), HttpStatus.OK);
  }
}