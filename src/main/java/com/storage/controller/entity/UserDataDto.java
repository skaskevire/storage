package com.storage.controller.entity;

public class UserDataDto {

  private Long primaryKey;
  private String name;
  private String description;
  private String updatedTimestamp;

  public UserDataDto(Long primaryKey, String name, String description,
      String updatedTimestamp) {
    this.primaryKey = primaryKey;
    this.name = name;
    this.description = description;
    this.updatedTimestamp = updatedTimestamp;
  }

  public UserDataDto() {
  }

  public Long getPrimaryKey() {
    return primaryKey;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getUpdatedTimestamp() {
    return updatedTimestamp;
  }
}
