package com.storage.util;

public enum UserDataHeaders {
  PRIMARY_KEY,
  NAME,
  DESCRIPTION,
  UPDATED_TIMESTAMP
}
