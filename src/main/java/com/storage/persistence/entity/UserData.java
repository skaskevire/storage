package com.storage.persistence.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserData {

  @Id
  @Column(name = "PRIMARY_KEY")
  private Long primaryKey;

  @Column(name = "NAME", length = 50)
  private String name;

  @Column(name = "DESCRIPTION", length = 50)
  private String description;

  @Column(name = "UPDATED_TIMESTAMP", updatable = false)
  private LocalDateTime updatedTimestamp;

  public UserData(Long primaryKey, String name, String description,
      LocalDateTime updatedTimestamp) {
    this.primaryKey = primaryKey;
    this.name = name;
    this.description = description;
    this.updatedTimestamp = updatedTimestamp;
  }

  public UserData() {
  }

  public Long getPrimaryKey() {
    return primaryKey;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public LocalDateTime getUpdatedTimestamp() {
    return updatedTimestamp;
  }
}
