package com.storage.persistence.repository;

import com.storage.persistence.entity.UserData;
import org.springframework.data.repository.CrudRepository;

public interface UserDataRepository extends CrudRepository<UserData, Long> {

}