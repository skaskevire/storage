package com.storage.csv.reader;

import com.storage.csv.entity.UserDataLine;
import com.storage.exception.TechnicalException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class MultipartFileReader {

  private static final String ROW_DELIMITER = ",";

  public List<UserDataLine> read(MultipartFile multipartFile) {
    List<UserDataLine> lines = new ArrayList<>();
    try (InputStream is = multipartFile.getInputStream();
        Scanner scanner = new Scanner(is)) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        lines.add(new UserDataLine(line.split(ROW_DELIMITER)));
        if (line.isEmpty()) {
          break;
        }
      }
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
    return lines;
  }
}
