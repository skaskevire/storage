package com.storage.csv.validator;

import static com.google.common.collect.Iterables.getLast;
import static com.storage.util.UserDataHeaders.PRIMARY_KEY;
import static com.storage.util.UserDataHeaders.UPDATED_TIMESTAMP;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import com.storage.csv.entity.UserDataLine;
import com.storage.csv.reader.MultipartFileReader;
import com.storage.exception.BusinessException;
import com.storage.util.UserDataHeaders;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class FileValidator {

  private static final Logger LOGGER = LoggerFactory.getLogger(MultipartFileReader.class);

  private static final String EMPTY_FILE = "Provided file is empty!";
  private static final String EMPTY_HEADERS = "Headers are empty!";
  private static final String EMPTY_LINE = "Provided line is empty!";
  private static final String MISSING_ENDING_LINE = "Ending line is not empty!";
  private static final String INVALID_HEADERS_MSG_PATTERN = "Specified headers are invalid, expected headers are: %s";
  private static final String WRONG_COL_NUM_MSG_PATTERN = "Line contains wrong number of columns: expected %s, actual %s";
  private static final String EMPTY_COLUMN_VALUE_MSG_PATTERN = "%s should not be empty!";
  private static final String INVALID_COLUMN_VALUE_MSG_PATTERN = "%s value %s is invalid!";

  private String dateTimeFormat;

  public FileValidator(@Value("${dateTimeFormat}") String dateTimeFormat) {
    this.dateTimeFormat = dateTimeFormat;
  }

  public void validateFileStructure(List<UserDataLine> lines) {
    if (lines.isEmpty()) {
      throw new BusinessException(EMPTY_FILE);
    }
    if (lines.get(0).size() == 0) {
      throw new BusinessException(EMPTY_HEADERS);
    }
    if (!getLast(lines).isEmpty()) {
      throw new BusinessException(MISSING_ENDING_LINE);
    }
    String[] actualHeaders = Stream.of(lines.get(0).getRows()).map(String::trim)
        .toArray(String[]::new);
    if (!Arrays.equals(Stream.of(UserDataHeaders.values()).map(UserDataHeaders::name).toArray(),
        actualHeaders)) {
      throw new BusinessException(format(INVALID_HEADERS_MSG_PATTERN,
          Arrays.toString(UserDataHeaders.values())));
    }
  }

  public boolean isLineValid(UserDataLine line) {
    if (line.isEmpty()) {
      LOGGER.info(EMPTY_LINE);
      return false;
    }

    return validateNumberOfColumns(line.size()) &&
        validatePrimaryKey(line.getRow(0)) &&
        validateUpdatedTimestamp(line.getRow(3));
  }

  private boolean validateNumberOfColumns(int actualLength) {
    if (actualLength != UserDataHeaders.values().length) {
      LOGGER.info(format(WRONG_COL_NUM_MSG_PATTERN,
          UserDataHeaders.values().length, actualLength));
      return false;
    }
    return true;
  }

  private boolean validatePrimaryKey(String pk) {
    if (isEmpty(pk)) {
      LOGGER.info(String.format(EMPTY_COLUMN_VALUE_MSG_PATTERN, PRIMARY_KEY.name()));
      return false;
    }
    try {
      Long.valueOf(pk);
    } catch (NumberFormatException e) {
      LOGGER.info(format(INVALID_COLUMN_VALUE_MSG_PATTERN, PRIMARY_KEY.name(), pk));
      return false;
    }

    return true;
  }

  private boolean validateUpdatedTimestamp(String updatedTimestamp) {
    if (isEmpty(updatedTimestamp)) {
      LOGGER.info(String.format(EMPTY_COLUMN_VALUE_MSG_PATTERN, UPDATED_TIMESTAMP.name()));
      return false;
    }

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
    try {
      LocalDateTime.parse(updatedTimestamp, formatter);
    } catch (DateTimeParseException e) {
      LOGGER.info(
          format(INVALID_COLUMN_VALUE_MSG_PATTERN, UPDATED_TIMESTAMP.name(), updatedTimestamp));
      return false;
    }

    return true;
  }

}
