package com.storage.csv.entity;

public class UserDataLine {

  private String[] rows;

  public UserDataLine(String[] rows) {
    this.rows = rows;
  }

  public String[] getRows() {
    return rows;
  }

  public String getRow(int index) {
    return rows[index];
  }

  public boolean isEmpty() {
    return rows.length == 0 || rows.length == 1 && getRow(0).isEmpty();
  }

  public int size() {
    return rows.length;
  }
}
