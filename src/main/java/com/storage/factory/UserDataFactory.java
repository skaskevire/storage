package com.storage.factory;

import com.storage.csv.entity.UserDataLine;
import com.storage.persistence.entity.UserData;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UserDataFactory {

  private String dateTimeFormat;

  public UserDataFactory(@Value("${dateTimeFormat}") String dateTimeFormat) {
    this.dateTimeFormat = dateTimeFormat;
  }

  public UserData create(UserDataLine line) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
    return new UserData(
        Long.valueOf(line.getRow(0)),
        line.getRow(1),
        line.getRow(2),
        LocalDateTime.parse(line.getRow(3), formatter));
  }

}
