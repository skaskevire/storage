package com.storage.factory;

import com.storage.controller.entity.UserDataDto;
import com.storage.persistence.entity.UserData;
import org.springframework.stereotype.Component;

@Component
public class UserDataDtoFactory {

  public UserDataDto create(UserData userData) {
    if (userData != null) {
      return new UserDataDto(userData.getPrimaryKey(), userData.getName(),
          userData.getDescription(), userData.getUpdatedTimestamp().toString());
    }
    return null;
  }

}
