package com.storage.service;

import static java.util.stream.Collectors.toList;

import com.storage.controller.entity.UserDataDto;
import com.storage.csv.entity.UserDataLine;
import com.storage.csv.validator.FileValidator;
import com.storage.factory.UserDataDtoFactory;
import com.storage.csv.reader.MultipartFileReader;
import com.storage.factory.UserDataFactory;
import com.storage.persistence.entity.UserData;
import com.storage.persistence.repository.UserDataRepository;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserDataServiceImpl implements UserDataService {

  private final MultipartFileReader multipartFileReader;
  private final UserDataDtoFactory userDataDtoFactory;
  private final UserDataRepository userDataRepository;
  private final FileValidator fileValidator;
  private final UserDataFactory userDataFactory;

  public UserDataServiceImpl(MultipartFileReader multipartFileReader,
      UserDataDtoFactory userDataDtoFactory,
      UserDataRepository userDataRepository,
      FileValidator fileValidator,
      UserDataFactory userDataFactory) {
    this.multipartFileReader = multipartFileReader;
    this.userDataDtoFactory = userDataDtoFactory;
    this.userDataRepository = userDataRepository;
    this.fileValidator = fileValidator;
    this.userDataFactory = userDataFactory;
  }

  @Override
  public void upload(MultipartFile multipartFile) {
    List<UserDataLine> lines = multipartFileReader.read(multipartFile);
    fileValidator.validateFileStructure(lines);
    List<UserData> userDataList = lines.stream()
        .skip(1)//skip header row
        .filter(fileValidator::isLineValid)
        .map(userDataFactory::create)
        .collect(toList());
    userDataRepository.saveAll(userDataList);
  }

  @Override
  public UserDataDto get(Long primaryKey) {
    return userDataRepository.findById(primaryKey)
        .map(userDataDtoFactory::create).orElse(null);
  }

  @Override
  public void delete(Long primaryKey) {
    userDataRepository.deleteById(primaryKey);
  }
}
