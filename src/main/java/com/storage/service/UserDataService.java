package com.storage.service;

import com.storage.controller.entity.UserDataDto;
import org.springframework.web.multipart.MultipartFile;

public interface UserDataService {

  void upload(MultipartFile multipartFile);

  UserDataDto get(Long primaryKey);

  void delete(Long primaryKey);
}
