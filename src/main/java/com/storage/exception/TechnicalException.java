package com.storage.exception;

public class TechnicalException extends RuntimeException {

  public TechnicalException(Throwable cause) {
    super(cause);
  }
}
